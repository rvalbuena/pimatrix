#
# "VU" meter display based on code from Stackoverflow
# * http://stackoverflow.com/questions/21762412/mpd-fifo-python-audioop-arduino-and-voltmeter-faking-a-vu-meter
# using the PiMatrix LED
# * http://mypishop.com/Pi%20MATRIX.html
# * http://w8bh.net/pi/PiMatrix1.pdf
# * https://bitbucket.org/rvalbuena/pimatrix

import os
import audioop
import time
import errno
import math
from collections import deque
from LEDMatrix import LEDMatrix

if __name__ == '__main__':

        our_pid = os.getpid()
        print our_pid

        #Open the FIFO that MPD has created for us
        #This represents the sample (44100:16:2) that MPD is currently "playing"
        fifo = os.open('/var/tmp/mpd.fifo', os.O_RDONLY)
        # note: /var/tmp is mapped to RAM in /etc/fstab
        lm = LEDMatrix()
        # the "fastSet" routines ignore orientation
        # but I'm using the Pi hdmi-connector side down and apparently that's "this side up"
        lm.ORIENTATION=0
        lm.enableLEDS()
        try:
                # create our buffer for the display values
                # there are probably faster data structures 
                # but this seems OK for now
                vol_queue = deque()
                # initialize it with zeros 
                for p in range(0,7):
                        vol_queue.append(0)

                while 1:
                        try:
                                rawStream = os.read(fifo, 4096)
                        except OSError as err:
                                if err.errno == errno.EAGAIN or err.errno == errno.EWOULDBLOCK:
                                        rawStream = None
                                else:
                                        raise

                        if rawStream:
                                # I'm really only interested in the stereo peak
                                #leftChannel = audioop.tomono(rawStream, 2, 1, 0)
                                #rightChannel = audioop.tomono(rawStream, 2, 0, 1)
                                stereoPeak = audioop.max(rawStream, 2)
                                #leftPeak = audioop.max(leftChannel, 2)
                                #rightPeak = audioop.max(rightChannel, 2)
                                #leftDB = 20 * math.log10(leftPeak) -74
                                #rightDB = 20 * math.log10(rightPeak) -74

                                # 'magic numbers' that will produce 0..8 for the led matrix
                                # note that reducing mpd volume via software mixer (mpc volume XX) 
                                # will also reduce these values, so better to use the speaker vol knobs
                                peak = (stereoPeak/4000)-1
                                #peak = (stereoPeak/3800)-1
                                vol_queue.append(0 if (peak < 0) else peak)
                                # print vol_queue
                                for m in range(0,8):
                                        # this produces a decent sine wave like pattern
                                        # fastSetLED ignores orientation
                                        # lm.fastSetLED(vol_queue[m],m)
                                        lm.setLED(vol_queue[m],m)
                                        # removing this pause actually produces a better looking
                                        # (more filled in) wave and does not seem to kill performance
                                        # (I think there are "sleeps" in some of the LED Matrix routin$
                                        # time.sleep(0.01)
                                # remove from the head of the deque
                                vol_queue.popleft()
                                # clear the display so that when we reach the end of the fifo
                                # (mpd stops playing/reaches the end of the playlist)
                                # we don't have anything left on the display
                                lm.turnOffLEDS()
        except (KeyboardInterrupt, SystemExit):
                lm.turnOffLEDS()
                #lm.disableLEDS()
        finally:
                # ensure that the leds turn off
                lm.disableLEDS()


