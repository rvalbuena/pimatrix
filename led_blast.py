from LEDMatrix import LEDMatrix
from time import sleep

delay=0.06

if __name__ == '__main__':
	lm = LEDMatrix()
	lm.debug=True
	lm.enableLEDS()
	try:
		while True:
			lm.setPattern(0b00011000,0b00011000)
			sleep(delay)	
			lm.setPattern(0b00111100,0b00111100)
			sleep(delay)	
			lm.setPattern(0b01100110,0b01100110)
			sleep(delay)	
			lm.setPattern(0b11100111,0b11100111)
			sleep(delay)	
			lm.setPattern(0b11000011,0b11000011)
			sleep(delay)	
#			lm.pulse4(numCycles=1)
#			lm.setPattern(0x02,0x02)
#			lm.setPattern(0x01,0x01)

	finally:
		# ensure that the leds turn off
		lm.disableLEDS()

