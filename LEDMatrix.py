#!/usr/bin/python

########################################################################
# 	
#	LEDMatrix: an "object oriented" conversion of Matrix3
#	* create a LEDMatrix() object and use its methods instead of calling functions directly
#	* changed global vars (delay, rows, columns) into properties of LEDMatrix object
#	* changed some high level functions (randomPixels, etc) to accept more parameters
#		such as variable delay
#	* changed demo to show Pulse1() in different corners, and disable leds on end
#
#	Original matrix-part3.py byBruce E. Hall 
#
#	Matrix3: Library of Pi Matrix display routines
#
#	This Python script contains a bunch of routines for driving
#	the Pi Matrix & creating interesting displays.
#	Online References: w8bh.net & mypishop.com
#	YouTube video at http://youtu.be/VbPBNmlGy34
#
#########################################################################


import smbus	#gives us a connection to the I2C bus
import time	#for timing delays
import random	#random number generator

########################################################################
#
# Lower level routines for bit-manipulation.
# Nothing here relates to our snazzy Pi Matrix.
#

def ReverseBits (byte):
	#reverse the bit order in the byte: bit0 <->bit 7, bit1 <-> bit6, etc.
	value = 0
	currentBit = 7
	for i in range(0,8):
		if byte & (1<<i):
			value |= (0x80>>i)
			currentBit -= 1
	return value

def ROR (byte):
	#perform a 'rotate right' command on byte
	#bit 1 is rotated into bit 7; everything else shifted right
	bit1 = byte & 0x01	#get right-most bit
	byte >>= 1		#shift right 1 bit
	if bit1:		#was right-most bit a 1?
		byte |= 0x80	#if so, rotate it into bit 7
	return byte

def ROL (byte):
	#perform a 'rotate left' command on byte
	#bit 7 is rotated into bit 1; everything else shifted left
	bit7 = byte & 0x080	#get bit7
	byte <<= 1		#shift left 1 bit
	byte &= 0xFF		#only keep 8 bits
	if bit7:		#was bit7 a 1?
		byte |= 0x01	#if so, rotate it into bit 1
	return byte


class LEDMatrix(object):

	#Definitions for the MCP23017 chip
	ADDR	= 0x20	#The I2C address of our chip
	DIRA	= 0x00	#PortA I/O direction, by pin. 0=output, 1=input
	DIRB	= 0x01	#PortB I/O direction, by pin. 0=output, 1=input
	PORTA 	= 0x12	#Register address for PortA
	PORTB 	= 0x13	#Register address for PortB
	
	ORIENTATION = 180	#default viewing angle for the pi & matrix
				#set this value according to your viewing angle
				#values are 0 (power jack on left)
				#  90 (power jack on bottom)
				# 180 (power jack on right)
				# 270 (power jack on top)
	rows 	= 0x00 		#starting pattern is (0,0) = all LEDS off
	columns	= 0x00 
	delay 	= 0.08		#time delay between LED display transitions
				#smaller values = faster animation
	debug = True

	def write(self,register,value):
		#Abstraction of I2C bus and the MCP23017 chip
		#Call with the chip data register & value pair
		#The chip address is constant ADDR (0x20).
		self.bus.write_byte_data(self.ADDR,register,value)

	def enableLEDS (self):
		#Set up the 23017 for 16 output pins
		self.write(self.DIRA, 0x00)	#all zeros = all outputs on PortA
		self.write(self.DIRB, 0x00)	#all zeros = all outputs on PortB

	def disableLEDS (self):
		#Set all outputs to high-impedance by making them inputs
		self.write(self.DIRA, 0xFF);	#all ones = all inputs on PortA
		self.write(self.DIRB, 0xFF);	#all ones = all inputs on PortB
	
	def turnOffLEDS (self): 
		#Clear the matrix display 
		self.write(self.PORTA, 0x00) 	#set all columns low
		self.write(self.PORTB, 0x00) 	#set all rows low

	def turnOnAllLEDS (self): 
		#Turn on all 64 LEDs 
		self.write(self.PORTA, 0xFF) 	#set all columns high
		self.write(self.PORTB, 0x00) 	#set all rows low
	
	def writeToLED (self,rowPins,colPins):
		#set logic state of LED matrix pins
		self.write(self.PORTA, 0x00)	#turn off all columns; prevent ghosting
		self.write(self.PORTB, rowPins)	#set rows first
		self.write(self.PORTA, colPins)	#now turn on columns

	########################################################################
	#
	# More low-level routines, saved for learning purposes.
	#
	
	def fastSetLED (self,row,col):
		#turn on an individual LED at (row,col). All other LEDS off.
		#ignores orientation. Unless speed is required, use SetLED instead
		self.bus.write_byte_data(self.ADDR,self.PORTA,0x80>>col)
		self.bus.write_byte_data(self.ADDR,self.PORTB,~(1<<row))
	
	def fastSetColumn (self,col):
		#turn on all LEDs in the specified column. Expects input of 0-7.
		#ignores orientation. Unless speed is required, use SetColumn.
		self.bus.write_byte_data(self.ADDR,self.PORTB,0x00)
		self.bus.write_byte_data(self.ADDR,self.PORTA,0x80>>col)
	
	def fastSetRow (self,row):
		#turn on all LEDs in the specified row. Expects input of 0-7.
		#ignores orientation. Unless speed is required, use SetRow.
		self.bus.write_byte_data(self.ADDR,self.PORTA,0xFF)
		self.bus.write_byte_data(self.ADDR,self.PORTB,~(1<<row))

	########################################################################
	#
	# Intermediate-level routines for
	# LED Pixel, Row, Column, and Pattern display.
	# Set constant ORIENTATION to 0,90,180,270 according to Matrix posn.
	#
	
	def setPattern (self,rowPattern,colPattern,orientation=ORIENTATION):
		#Applies given row & column patterns to the matrix.
		#For columns, bit 0 is left-most and bit 7 is at far right.
		#For rows, bit 0 is at the top and bit 7 is at the bottom.
		#Example: (0x07,0x03) will set 3 row bits & 2 columns bits,
		#forming a rectagle of 6 lit LEDS in upper left corner of
		#the matrix, three rows tall and two columns wide.
		#Why? 0x07 = 0b00000111 (three lower row bits set).
		#     0x03 = 0b00000011 (two lower column bits set).
	
		#global rows, columns	#save current row/column
		self.rows = rowPattern
		self.columns = colPattern
	
		if orientation==0:
			self.writeToLED(~self.rows,ReverseBits(self.columns))
		elif orientation==90:
			self.writeToLED(~self.columns,self.rows)
		elif orientation==180:
			self.writeToLED(~ReverseBits(self.rows),self.columns)
		elif orientation==270:
			self.writeToLED(~ReverseBits(self.columns),ReverseBits(self.rows))
	
	def setLED(self,row,col):
		#turn on an individual LED at (row,col). All other LEDS off.
		#expects inputs of (0,0) to (7,7).
		self.setPattern(1<<row,1<<col)
	
	def setColumn(self,col):
		#turn on all LEDs in the specified column. Expects input of 0-7.
		self.setPattern(0xFF,1<<col)
	
	def setRow(self,row):
		#turn on all LEDs in the specified row. Expects input of 0-7.
		self.setPattern(1<<row,0xFF)
	
	def moveDown(self):
		#shifts entire display downward by one pixel
		self.setPattern(self.rows<<1,self.columns)
	
	def moveUp(self):
		#shifts entire display downward by one pixel
		self.setPattern(self.rows>>1,self.columns)
	
	def moveRight(self):
		#shifts entire display one pixel to the right
		self.setPattern(self.rows,self.columns<<1)
	
	def moveLeft(self):
		#shifts entire display one pixel to the left
		self.setPattern(self.rows,self.columns>>1)

	########################################################################
	#
	# Higher-level routines for playing with the Pi Matrix board.
	# Each routine does a simple display animation, calling
	# one or more of the low-level routines above.
	# Set property DELAY to change animation speed (default 0.08)
	#
	
	def showRoutine (self,name):
		#display the routine name to console, file, or wherever you want.
		#this is useful for creating a log of your display.
		if (self.debug == True):
			print " ",name
	
	def flashLEDS (self):
		#Flash all of the LEDS on/off for the specified time
		self.turnOnAllLEDS()
		time.sleep(self.delay)
		self.turnOffLEDS()
		time.sleep(self.delay)
	
	def multiRowCylon (self,pattern=0x55,numCycles=4,delay=0.08):
		#Do a side-to-side LED chaser using multiple rows
		self.showRoutine("%d Row Cylon %x" % (numCycles,pattern))
		for count in range(0,numCycles):
			for col in range(0,8):
				self.setPattern(pattern,1<<col)
				time.sleep(delay)
			for col in range(6,0,-1):
				self.setPattern(pattern,1<<col)
				time.sleep(delay)
	
	def singleRowCylon (self,row=0,numCycles=4):
		#Do a side-to-side LED chaser on a single row
		self.multiRowCylon(1<<row,numCycles)
	
	def allRowCylon (self,numCycles=4):
		#Do a side-to-side LED chaser using all rows
		self.multiRowCylon(0xFF,numCycles)
	
	def multiColumnCylon (self,pattern=0x55,numCycles=4,delay=0.08):
		#Do a back-and-forth LED chaser on multiple columns
		self.showRoutine("%d Column cylon %x" % (numCycles,pattern))
		for count in range(0,numCycles):
			for row in range(0,8):
				self.setPattern(1<<row,pattern)
				time.sleep(delay)
			for row in range(6,0,-1):
				self.setPattern(1<<row,pattern)
				time.sleep(delay)
	
	def multiBarCylon (self,pattern=0x77,numCycles=4,delay=0.08):
		#Do a back-and-forth LED chaser on multiple columns
		self.showRoutine("%d Bar cylon %x" % (numCycles,pattern))
		for count in range(0,numCycles):
			for row in range(0,8):
				self.setPattern(~1<<row,pattern)
				time.sleep(delay)
			for row in range(6,0,-1):
				self.setPattern(~1<<row,pattern)
				time.sleep(delay)
	
	def singleColumnCylon (self,col=0,numCycles=4):
		#Do an up-and-down LED chaser on a single column
		self.multiColumnCylon(1<<col,numCycles)
	
	def allColumnCylon (self,numCycles=4):
		#Do an up-and-down LED chaser using all columns
		self.multiColumnCylon(0xFF,numCycles)
	
	def marquis (self,indent=0,numCycles=8,delay=0.02):
		#do a LED chaser around perimeter of matrix
		self.showRoutine("%d Marquis #%d at %g" % (numCycles,indent,delay))
		for count in range(0,numCycles):
			for col in range(0+indent,8-indent):
				self.setLED(0+indent,col)
				time.sleep(delay)
			for row in range(1+indent,7-indent):
				self.setLED(row,7-indent)
				time.sleep(delay)
			for col in range(7-indent,-1+indent,-1):
				self.setLED(7-indent,col)
				time.sleep(delay)
			for row in range(6-indent,0+indent,-1):
				self.setLED(row,0+indent)
				time.sleep(delay)
	
	def rotateDisplay (self,direction, numCycles=32,delay=0.08):
		#rotates current display in desired direction
		#global rows,columns
		self.showRoutine("%d Rotate display %s" %(numCycles,direction))
		time.sleep(delay)	#initial pattern = first cycle
		for count in range(0,numCycles):
			if direction=="down":
				self.rows = ROL(self.rows)
			elif direction=="up":
				self.rows = ROR(self.rows)
			elif direction=="right":
				self.columns = ROL(self.columns)
			elif direction=="left":
				self.columns = ROR(self.columns)
			self.setPattern(self.rows,self.columns)
			time.sleep(delay)
	
	def randomPatterns (self,numCycles=32):
		#puts random display patterns on the LED matrix
		self.showRoutine("%d Random Patterns" % numCycles)
		delay = 0.05
		for count in range(0,numCycles):
			rowPattern = random.randint(0,255)
			colPattern = random.randint(0,255)
			self.setPattern(rowPattern,colPattern)
			time.sleep(delay)
	
	#def randomPixels (self,numCycles=128):
	def randomPixels(self,maxCol=7,maxRow=7,numCycles=128,delay=0.02):
		#puts random display patterns on the LED matrix
		self.showRoutine("%d Random Pixels" % numCycles)
		#delay = 0.02
		for count in range(0,numCycles):
			#row = random.randint(0,7)
			row = random.randint(0,maxRow)
			#col = random.randint(0,7)
			col = random.randint(0,maxCol)
			self.setLED (row,col)
			time.sleep(delay)
	
	def showEyes (self):
		self.setPattern(0x18,0x66)
	
	def blinkEyes (self,numCycles=8,delay=0.5):
		for count in range(0,numCycles):
			self.showEyes()
			time.sleep(delay)
			self.turnOffLEDS()
			time.sleep(delay *0.2)
	
	def blink (self,numCycles=8,delay=0.5):
		#flashes current display off, then back on
		for count in range(0,numCycles):
			time.sleep(delay)
			self.disableLEDS()
			time.sleep(delay)
			self.enableLEDS()
	
	def figure8 (self,numCycles=4,delay=0.05):
		self.showRoutine("%d Figure8" % numCycles)
		self.setPattern(3,3)
		time.sleep(delay*3)
		for cycle in range(0,numCycles):
			for count in range(0,6):
				self.moveRight()
				time.sleep(delay)
			for count in range(0,6):
				self.moveDown()
				self.moveLeft()
				time.sleep(delay)
			for count in range(0,6):
				self.moveRight()
				time.sleep(delay)
			for count in range(0,6):
				self.moveUp()
				self.moveLeft()
				time.sleep(delay)
	
	def pawCircle(self,delay=0.5):
		self.showRoutine("Paw Circle")
		self.setPattern(0x33,0x03)
		time.sleep(delay)
		for count in range(0,3):
			self.moveRight()
			self.moveRight()
			time.sleep(delay)
		self.setPattern(0xCC,0xC0)
		time.sleep(delay)
		for count in range(0,3):
			self.moveLeft()
			self.moveLeft()
			time.sleep(delay)
	
	def pulse1(self,numCycles=8,delay=0.50,orientation=ORIENTATION):
		#pulsing animation in corner of display
		#change orientation to change the corner used
		self.showRoutine("%d Pulse1" % numCycles)
		for count in range(0,numCycles):
			self.setPattern(0x80,0x01,orientation)
			time.sleep(0.03)
			self.setPattern(0xC0,0x03,orientation)
			time.sleep(0.03)
			self.setPattern(0xE0,0x07,orientation)
			time.sleep(0.03)
			self.setPattern(0xC0,0x03,orientation)
			time.sleep(0.03)
			self.setPattern(0x80,0x01,orientation)
			time.sleep(delay-0.12)
	
	def pulse4(self,numCycles=8,delay=0.50):
		#pulsing animation in all four display corners
		self.showRoutine("%d Pulse4" % numCycles)
		for count in range(0,numCycles):
			self.setPattern(0x81,0x81)
			time.sleep(0.03)
			self.setPattern(0xC3,0xC3)
			time.sleep(0.03)
			self.setPattern(0xE7,0xE7)
			time.sleep(0.03)
			self.setPattern(0xC3,0xC3)
			time.sleep(0.03)
			self.setPattern(0x81,0x81)
			time.sleep(delay-0.12)
	
	def rotateBall(self):
		#draw a 3x3 ball on display & move it around
		self.showRoutine("Rotate Ball")
		self.setPattern(0x0E,0x0E)
		self.rotateDisplay("right")
		self.rotateDisplay("left")
		self.rotateDisplay("up")
		self.rotateDisplay("down")
	
	def multiplexDisplay (z,count=40):
		# call this routine with Z, the image to display on the matrix
		# Z is a list containing 8 bytes (8x8=64 bits for 64 LEDs)
		# z[0] is data for the top row of the display; z[7] is bottom row.
		# count is number of times to cycle the display
		for count in range(0,count):
			for row in range(0,8):
				self.setPattern(1<<row,z[row])	#quickly display each row
	
	def trySomeRoutines(self):
		self.pulse1(orientation=0)
		self.pulse1(orientation=90)
		self.pulse1(orientation=180)
		self.pulse1(orientation=270)
		self.pulse4()
		self.singleRowCylon()
		self.multiRowCylon()
		self.allRowCylon()
		self.singleColumnCylon()
		self.multiColumnCylon()
		self.allColumnCylon()
		self.marquis()
		self.rotateBall()
		self.figure8()
		self.pawCircle()
		self.randomPatterns()
		self.randomPixels()
	

	def __init__(self):
		# bus 0 for 256MB Pi, bus 1 for 512MB
		self.bus = smbus.SMBus(0)
	
if __name__ == '__main__':
	lm = LEDMatrix()
	lm.enableLEDS()
	try:
		lm.trySomeRoutines()
	finally:
		# ensure that the leds turn off
		lm.disableLEDS()
