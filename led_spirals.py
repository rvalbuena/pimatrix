#
# Spiral in and out effect using the LEDMatrix.marquis by changing the indent
#

from LEDMatrix import LEDMatrix
#from time import sleep
#from datetime import datetime
#import os

if __name__ == '__main__':

	lm = LEDMatrix()
	lm.enableLEDS()
	lm.debug=False
	delay = 0.04
	cycles = 4

	try:
		while True:
			# loop around the outer edge between cycles
			lm.marquis(numCycles=1,delay=delay)
			# spiral inward by increasing marquee indent
			for i in range(0,4):
#				print (i*3/2)
#				lm.marquis(indent=i,numCycles=i+1,delay=delay)
				# the numCycles math makes the "spiral in" look a little 
				# different than the "spiral out" 
				lm.marquis(indent=i,numCycles=(i*3/2),delay=delay)
			# outward spirals
			for j in range(4,0,-1):
#				lm.marquis(indent=j,numCycles=j+1,delay=delay)
				lm.marquis(indent=j,numCycles=j,delay=delay)
	except (KeyboardInterrupt, SystemExit):
		lm.turnOffLEDS()
		lm.disableLEDS()
	finally:
		lm.disableLEDS()
